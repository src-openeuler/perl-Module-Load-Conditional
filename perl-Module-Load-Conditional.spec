%define mod_name Module-Load-Conditional
Name:           perl-%{mod_name}
Version:        0.74
Release:        3
Summary:        Looking up module information / loading at runtime
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/B/BI/BINGOS/%{mod_name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  perl-generators perl coreutils perl-interpreter perl(ExtUtils::MakeMaker) perl(Carp) perl(version)
BuildRequires:  perl(Config) perl(constant) perl(Exporter) perl(File::Spec) perl(FileHandle) perl(Locale::Maketext::Simple)
BuildRequires:  perl(Module::CoreList) perl(Module::Load) perl(Module::Metadata) perl(Params::Check) perl(strict) perl(vars)
#tests
BuildRequires:  perl(FindBin) perl(lib) perl(Test::More) perl(warnings)

Requires:       perl(Config) perl(Module::CoreList) perl(Module::Load) perl(Module::Metadata) perl(version)

%description
Module::Load::Conditional provides simple ways to query and possibly load 
any of the modules you have installed on your system during runtime. It is 
able to load multiple modules at once or none at all if one of them was not 
able to load. It also takes care of any error checking and so forth.

%package_help

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test 

%files
%doc README CHANGES
%{perl_vendorlib}/*

%files help
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.74-3
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Oct 25 2022 zhoushuiqing <zhoushuiqing2@huawei.com> - 0.74-2
- define mod_name to opitomize the specfile

* Tue Jan 26 2021 liudabo <liudabo1@huawei.com> - 0.74-1
- upgrade version to 0.74

* Sat Jul 25 2020 zhanzhimin <zhanzhimin@huawei.com> - 0.72-1
- Bump to version 0.72

* Thu Sep 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.68-418
- Package init
